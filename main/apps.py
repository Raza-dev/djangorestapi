from django.apps import AppConfig
# from main.btc_scheduler import btc_updater


class MainConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'main'

    def ready(self):
        print("BTC Scheduler ...")
        from main.btc_scheduler import btc_updater
        btc_updater.start()
