from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from . serializers import BtcSerializer, UserSerializer
from . models import *
from decouple import config
import requests
import pandas as pd
import json
# Create your views here.


class QuotesViewSet(viewsets.ModelViewSet):
    serializer_class = BtcSerializer

    def get_queryset(self):
        data = BtcPrice.objects.all()
        return data

    def _format_json_data(self, data):
        response_data = pd.DataFrame.from_dict(data, orient='index')
        response_data = response_data.reset_index()
        response_data = response_data.rename(columns={"1. From_Currency Code": "from_currency_code",
                                                      "2. From_Currency Name": "from_currency_name", "3. To_Currency Code": "to_currency_code", "4. To_Currency Name": "to_currency_name", "5. Exchange Rate": "exchange_rate", "6. Last Refreshed": "last_refreshed", "7. Time Zone": "time_zone", "8. Bid Price": "bid_price", "9. Ask Price": "ask_price"})
        return response_data

    def _get_btc_data(self):
        url = config("API_URL") + "&apikey=" + config("ALPHA_API_KEY")
        api_request = requests.get(url)

        try:
            api_request.raise_for_status()
            return api_request.json()

        except:
            return None

    def save_btc_data(self):

        # print("Raza")
        # Get API Repsonse Data
        btc_data = self._get_btc_data()

        # Pass Data to a function to rename the dictionary keys using pandas
        get_data = self._format_json_data(btc_data)

        if get_data is not None:
            try:
                btc_object = BtcPrice.objects.create(
                    from_currency_code=get_data["from_currency_code"].values[0], from_currency_name=get_data["from_currency_name"].values[0], to_currency_code=get_data["to_currency_code"].values[0], to_currency_name=get_data["to_currency_name"].values[0], exchange_rate=get_data["exchange_rate"].values[0], current_date=get_data["last_refreshed"].values[0])
                btc_data.save()

            except:
                pass
