from django.db import models
from datetime import datetime

# Create your models here.


class BtcPrice(models.Model):
    from_currency_code = models.CharField(max_length=60)
    from_currency_name = models.CharField(max_length=60)
    to_currency_code = models.CharField(max_length=60)
    to_currency_name = models.CharField(max_length=60)
    exchange_rate = models.DecimalField(
        max_digits=20, decimal_places=10)
    current_date = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.from_currency_name
