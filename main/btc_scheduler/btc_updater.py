from apscheduler.schedulers.background import BackgroundScheduler
from main.views import QuotesViewSet


def start():
    scheduler = BackgroundScheduler()
    btc = QuotesViewSet()
    scheduler.add_job(btc.save_btc_data)
    scheduler.add_job(btc.save_btc_data, "interval",
                      minutes=60, id="btc_001", replace_existing=True)
    scheduler.start()
