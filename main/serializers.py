
from collections import OrderedDict
from rest_framework import serializers
from django.contrib.auth.models import User, Group

from . models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class BtcSerializer(serializers.ModelSerializer):
    class Meta:
        model = BtcPrice
        fields = '__all__'
